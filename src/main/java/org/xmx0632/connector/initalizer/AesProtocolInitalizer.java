package org.xmx0632.connector.initalizer;

import org.springframework.stereotype.Component;

import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;

/**
 * Aes Protocol Initalizer
 * 
 */
@Component
public class AesProtocolInitalizer extends BaseProtocolInitalizer {

	@Override
	protected void initChannel(SocketChannel ch) throws Exception {

		ChannelPipeline pipeline = ch.pipeline();

		setupBasePipeline(pipeline, true);
	}

}
