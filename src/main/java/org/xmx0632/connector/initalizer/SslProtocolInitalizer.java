package org.xmx0632.connector.initalizer;

import org.springframework.stereotype.Component;

import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.SelfSignedCertificate;

/**
 * Ssl Protocol Initalizer
 * 
 */
@Component
public class SslProtocolInitalizer extends BaseProtocolInitalizer {

	private SslContext sslCtx = null;

	@Override
	protected void initChannel(SocketChannel ch) throws Exception {
		SelfSignedCertificate ssc = new SelfSignedCertificate();

		sslCtx = SslContextBuilder.forServer(ssc.certificate(), ssc.privateKey()).build();

		ChannelPipeline pipeline = ch.pipeline();

		pipeline.addLast(sslCtx.newHandler(ch.alloc()));

		setupBasePipeline(pipeline, false);
	}

}
