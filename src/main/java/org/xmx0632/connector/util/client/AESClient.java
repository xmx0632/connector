package org.xmx0632.connector.util.client;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Client for Test
 * 
 */
public class AESClient {

	public static void main(String[] args) throws IOException {
		Socket socket = null;
		OutputStream out = null;

		try {
			socket = new Socket("localhost", 2002);
			out = socket.getOutputStream();

			// 请求服务器
			String lines = "0002\r\n8888\r\n8889\r\n";
			byte[] outputBytes = lines.getBytes("UTF-8");
			out.write(outputBytes);
			out.flush();

		} finally {
			// 关闭连接
			out.close();
			socket.close();
		}

	}

}
