package org.xmx0632.connector.util.client;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 * {"code":"0002"}
 * 
 *
 */
public class TcpClient {

	public static void main(String[] args) throws Exception {
		new TcpClient("localhost", 2003).run();
	}

	private final String host;
	private final int port;

	public TcpClient(String host, int port) {
		this.host = host;
		this.port = port;
	}

	public void run() throws Exception {
		EventLoopGroup group = new NioEventLoopGroup();
		try {
			Bootstrap bootstrap = new Bootstrap().group(group).channel(NioSocketChannel.class)
					.handler(new TcpClientInitializer(1,1));
			Channel channel = bootstrap.connect(host, port).sync().channel();
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			while (true) {
				String line = in.readLine();
				System.out.println("::" + line);
				String command = CommandUtil.getCommand(line);
				System.out.println("request:" + command);
				channel.writeAndFlush(command + "\r\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			group.shutdownGracefully();
		}

	}


}
