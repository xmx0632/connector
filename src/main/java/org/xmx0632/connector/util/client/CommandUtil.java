package org.xmx0632.connector.util.client;

public class CommandUtil {
	public static String getCommand(String line) {
		return getCommand(line, 0);
	}

	public static String getCommand(String line, int counter) {
		String command = line;
		long currentTimeMillis = counter != 0 ? counter : System.currentTimeMillis();
		String version = "0.0.2";

		String regCommand = "{\"code\":\"0001\", \"gateway\":{\"gatewayId\":\"10000\"},\"timestamp\":%d,\"version\":\"%s\"}";
		String heartbeatCommand = "{\"code\":\"0002\",\"gateway\":{\"gatewayId\":\"10000\"},\"timestamp\":%d,\"version\":\"%s\"}";
		String alarmCommand = "{\"code\":\"A001\",\"gateway\":{\"gatewayId\":\"10000\"},\"timestamp\":%d,\"version\":\"%s\"}";

		switch (line) {
		case "register":
			command = String.format(regCommand, currentTimeMillis, version);
			break;

		case "heartbeat":
			command = String.format(heartbeatCommand, currentTimeMillis, version);
			break;

		case "alarm":
			command = String.format(alarmCommand, currentTimeMillis, version);
			break;

		default:
			break;
		}
		return command;

	}
}
