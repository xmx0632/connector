package org.xmx0632.connector.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AesUtil {
	private static final Logger LOG = LoggerFactory.getLogger(AesUtil.class);

	// TODO 实现AES解密算法,解出json字符串
	public static String decrypt(String accessKey, String data) {
		LOG.debug("accessKey:{},data:{}", accessKey, data);
		if (data.contains("0001")) {
			return "{\"name\":\"demo8888Message\"}";
		}
		return data;
	}

}
