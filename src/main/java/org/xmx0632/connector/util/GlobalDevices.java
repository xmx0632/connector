package org.xmx0632.connector.util;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.xmx0632.connector.model.Device;

public class GlobalDevices {

	private static Map<String, Device> map = new ConcurrentHashMap<String, Device>(20000);

	public static boolean connect(String deviceId, Device device) {
		Device obj = map.put(deviceId, device);
		return obj != null;
	}

	public static boolean disconnect(String deviceId) {
		Device obj = map.remove(deviceId);
		return obj != null;
	}
}
