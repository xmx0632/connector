package org.xmx0632.connector.protocol.processor;

public interface MessageProcessor<E,R> {
	
	String CRTL = "\r\n";

	public R execute(E e);
}
