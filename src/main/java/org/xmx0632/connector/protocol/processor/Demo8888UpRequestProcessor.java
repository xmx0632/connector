package org.xmx0632.connector.protocol.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.xmx0632.connector.protocol.message.Demo8888UpRequest;
import org.xmx0632.connector.protocol.message.Demo8888UpResponse;

@Component
public class Demo8888UpRequestProcessor extends BaseMessageProcessor<Demo8888UpRequest, Demo8888UpResponse> {

	public static final Logger LOG = LoggerFactory.getLogger(Demo8888UpRequestProcessor.class);

	@Override
	public Demo8888UpResponse process(final Demo8888UpRequest message) {
		LOG.debug("handle invoked message:{}", message);

		messageSender.send(message);

		return new Demo8888UpResponse();
	}

}
