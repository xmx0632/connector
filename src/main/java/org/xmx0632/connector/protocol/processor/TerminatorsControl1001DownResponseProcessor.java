package org.xmx0632.connector.protocol.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.xmx0632.connector.protocol.message.Response;
import org.xmx0632.connector.protocol.message.TerminatorsControl1001DownResponse;

/**
 * 5.5	控制指令执行结果上报流程
 * 
 * 处理接收到的控制消息的响应结果
 *
 */
@Component
public class TerminatorsControl1001DownResponseProcessor
		extends BaseMessageProcessor<TerminatorsControl1001DownResponse, Response> {

	public static final Logger LOG = LoggerFactory.getLogger(TerminatorsControl1001DownResponseProcessor.class);

	@Override
	public Response process(TerminatorsControl1001DownResponse message) {
		LOG.debug("process message:{}", message);
		// TODO 接收响应结果后,更新数据库中的下发消息响应结果(可以考虑异步实现),清理内存中指令信息

		// 不返回响应消息
		return Response.NO_RESPONSE;
	}
}
