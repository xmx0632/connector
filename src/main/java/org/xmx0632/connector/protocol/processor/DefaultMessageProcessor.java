package org.xmx0632.connector.protocol.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.xmx0632.connector.protocol.message.Request;
import org.xmx0632.connector.protocol.message.Response;

/**
 * default message handler
 *
 */
@Component
public class DefaultMessageProcessor extends BaseMessageProcessor<Request,Response> {

	public static final Logger LOG = LoggerFactory.getLogger(DefaultMessageProcessor.class);

	@Override
	public Response process(Request message) {
		LOG.debug("default handle invoked message:{}", message.getClass());
		return Response.NO_RESPONSE;
	}

}
