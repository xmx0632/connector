package org.xmx0632.connector.protocol.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.xmx0632.connector.protocol.message.DefaultResponse;
import org.xmx0632.connector.protocol.message.Message;
import org.xmx0632.connector.service.MessageSender;

/**
 * base message handler
 *
 */
public class BaseMessageProcessor<I extends Message,O extends Message> extends AbstractMessageProcessor<I,O> {

	public static final Logger LOG = LoggerFactory.getLogger(BaseMessageProcessor.class);

	@Autowired
	@Qualifier("messageSender")
	protected MessageSender messageSender;
	
	@SuppressWarnings("unchecked")
	public O process(I requestMessage) {
		LOG.debug("default handle invoked message:{}", requestMessage.getClass());
		return (O)new DefaultResponse();
	}

}
