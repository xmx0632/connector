package org.xmx0632.connector.protocol.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmx0632.connector.protocol.message.Message;

public abstract class AbstractMessageProcessor<I extends Message,O extends Message> implements MessageProcessor<I,O> {

	public static final Logger LOG = LoggerFactory.getLogger(AbstractMessageProcessor.class);

	public abstract O process(I message);

	public O execute(I message) {
		O response = process(message);
		return response;
	}
}
