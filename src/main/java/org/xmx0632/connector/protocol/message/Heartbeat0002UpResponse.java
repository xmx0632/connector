package org.xmx0632.connector.protocol.message;

import com.google.common.base.MoreObjects;

/**
 * {"version": "0.0.1", "code": "8002","result": 0,"timestamp": 1414565197}
 */
public class Heartbeat0002UpResponse extends Response {

	private static final long serialVersionUID = 1L;

	private static final String CODE = "8002";

	private int result = RESULT_SUCCESS;

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	@Override
	public String code() {
		return CODE;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("CODE", CODE).add("result", result).toString();
	}

}
