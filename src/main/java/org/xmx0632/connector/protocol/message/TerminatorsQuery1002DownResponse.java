package org.xmx0632.connector.protocol.message;

import com.google.common.base.MoreObjects;

/**
 * TODO 命令码： 9002
 * 
 *
 */
public class TerminatorsQuery1002DownResponse extends Response {

	private static final long serialVersionUID = 1632569677310690377L;

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("code", code).toString();
	}

	@Override
	public String code() {
		return "9002";
	}

}
