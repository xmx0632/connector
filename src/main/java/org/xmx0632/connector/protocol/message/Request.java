package org.xmx0632.connector.protocol.message;

import com.google.common.base.MoreObjects;

public class Request extends BaseMessage {

	private static final long serialVersionUID = 1L;

	protected Gateway gateway;

	public Gateway getGateway() {
		return gateway;
	}

	public void setGateway(Gateway gateway) {
		this.gateway = gateway;
	}

	@Override
	public String code() {
		return "0000";
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("gateway", gateway).toString();
	}

	
}