package org.xmx0632.connector.protocol.message;

import java.util.List;

import com.google.common.collect.Lists;

/**
 * 
 * TODO 命令码： 1002
 * 
 * 功能： 对终端设备进行状态查询
 * 
 * 方向： 云平台→网关
 * 
 * 说明：该命令用于控制现场网络内的终端设备进行状态查询。共性平台的控制命令发出后 10 秒内未 收到网关的响应则认为操作失败，可重新操作 3 次，如果 3
 * 次均无响应则断开当前 TCP 连接。
 *
 */
public class TerminatorsQuery1002DownRequest extends Request {

	private static final long serialVersionUID = 3109136274215017152L;
	private String sequence = "";
	private List<QueryDevice> deviceList = Lists.newArrayList();

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	public List<QueryDevice> getDeviceList() {
		return deviceList;
	}

	public void setDeviceList(List<QueryDevice> deviceList) {
		this.deviceList = deviceList;
	}

	@Override
	public String code() {
		return "1002";
	}

}

class AtNumber {
	private String atnumber;
	private String strvalue;
	private Float value;

	public AtNumber() {
	}

	public AtNumber(String atnumber) {
		this.atnumber = atnumber;
	}

	public String getAtnumber() {
		return atnumber;
	}

	public void setAtnumber(String atnumber) {
		this.atnumber = atnumber;
	}

	public String getStrvalue() {
		return strvalue;
	}

	public void setStrvalue(String strvalue) {
		this.strvalue = strvalue;
	}

	public Float getValue() {
		return value;
	}

	public void setValue(Float value) {
		this.value = value;
	}

}

class QueryDevice {
	private String deviceId;
	private List<AtNumber> query = Lists.newArrayList();

	public QueryDevice() {
	}

	public QueryDevice(String deviceId, List<AtNumber> query) {
		this.deviceId = deviceId;
		this.query = query;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public List<AtNumber> getQuery() {
		return query;
	}

	public void setQuery(List<AtNumber> query) {
		this.query = query;
	}

}