package org.xmx0632.connector.protocol.message;

public abstract class Response extends BaseMessage {
	private static final long serialVersionUID = 1L;

	public static final int RESULT_SUCCESS = 0;
	public static final int RESULT_FAIL = 1;

	public static final Response CLOSE_CONNECTION = new Response() {
		private static final long serialVersionUID = 4538779558874488483L;

		@Override
		public String code() {
			return "close_connection";
		}
	};

	public static Response NO_RESPONSE = new Response() {
		private static final long serialVersionUID = 4538779558874488483L;

		@Override
		public String code() {
			return "no_response";
		}
	};

	public Response() {
	}

	public abstract String code();

}
