package org.xmx0632.connector.protocol.message;

import com.google.common.base.MoreObjects;

/**
 * TODO 命令码： 0xA001
 * 
 * 终端设备上报报警信息
 * 
 * 命令码： 0xA001
 * 
 * 功能： 网关上报终端设备报警信息
 * 
 * 方向： 网关→共性平台
 *
 * { "code": "A001", "result": 0, "timestamp": "20150915153018" }/r/n
 */
public class ReportAlarmA001UpResponse extends Response {

	private static final long serialVersionUID = 7801719636352843086L;

	private static final String CODE = "A001";

	@Override
	public String code() {
		return CODE;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("CODE", CODE).toString();
	}

}
