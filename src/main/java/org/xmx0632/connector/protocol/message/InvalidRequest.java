package org.xmx0632.connector.protocol.message;

/**
 * 请求包解析错误时统一封装为这个消息
 *
 */
public class InvalidRequest extends Request {

	private static final long serialVersionUID = -2724007871330152812L;
	private String msg = "invalid request";

	private static final String CODE = "-1111";

	@Override
	public String code() {
		return CODE;
	}

	public InvalidRequest(String msg) {
		this.msg = msg;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
