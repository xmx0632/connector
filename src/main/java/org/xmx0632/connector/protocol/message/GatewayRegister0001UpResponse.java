package org.xmx0632.connector.protocol.message;

import com.google.common.base.MoreObjects;

// TODO 网关注册 
/**
 * 
 * { "code": "8001", "result": 0, "timestamp": 1414565197 }/r/n
 *
 */
public class GatewayRegister0001UpResponse extends Response {

	private static final long serialVersionUID = -8615816843140789218L;

	private static final String CODE = "8001";

	private int result = 0;

	public GatewayRegister0001UpResponse(boolean registerSuccess) {
		result = registerSuccess ? 0 : 1;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	@Override
	public String code() {
		return CODE;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("CODE", CODE).add("result", result).toString();
	}

}
