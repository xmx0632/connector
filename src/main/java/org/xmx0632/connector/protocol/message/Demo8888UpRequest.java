package org.xmx0632.connector.protocol.message;

public class Demo8888UpRequest extends Request {

	private static final long serialVersionUID = 7168980391528275601L;

	private static final String CODE = "8888";

	@Override
	public String code() {
		return CODE;
	}

}
