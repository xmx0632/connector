package org.xmx0632.connector.protocol.message;

import java.io.Serializable;

import com.alibaba.fastjson.annotation.JSONField;
import com.google.common.base.MoreObjects;

public abstract class BaseMessage implements Message, Serializable {

	private static final long serialVersionUID = 1L;
	protected String version = "0.0.1";
	protected final String code = "0000";
	protected long timestamp;

	protected String remoteAddress = "";

	@JSONField(serialize = false)
	public String getRemoteAddress() {
		return remoteAddress;
	}

	public void setRemoteAddress(String remoteAddress) {
		this.remoteAddress = remoteAddress;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getCode() {
		return code();
	}

	public abstract String code();

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("version", version).add("code", code).add("timestamp", timestamp)
				.add("remoteAddress", remoteAddress).toString();
	}

}