package org.xmx0632.connector.protocol.message;

import com.google.common.base.MoreObjects;

// TODO 终端控制结果响应消息

/**
 * { "code": "9001", "sequence": "a89e91", "gateway": { "gatewayId": "10000", },
 * "deviceList": [ { "deviceId": "AABB", "ctrlResult": 0, "stream": [ {
 * "atnumber": "0001", "result": 0, "value": "20151225" }, { "atnumber": "0002",
 * "result": 0, "value": 75.3 } ] }, { "deviceId": "CCDD", "ctrlResult": 0,
 * "stream": [ { "atnumber": "0001", "result": 0, "value": "20151225" }, {
 * "atnumber": "0002", "result": 0, "value": "75.3 } ] } ], "timestamp":
 * 1414565197}/r/n
 *
 */
public class TerminatorsControl1001DownResponse extends Response {

	private static final long serialVersionUID = 8008246602701419930L;

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("code", code).toString();
	}

	@Override
	public String code() {
		return "9001";
	}

}
