package org.xmx0632.connector.protocol.message;

import com.google.common.base.MoreObjects;

// TODO 网关注册 
// 网关与共性平台建立长连接后，需要立刻发送“网关注册”包，同时使用网关响应包中的时间
// 戳校准本地时间。共性平台正常响应后才可开始下一步的通信。
// 网关→共性平台

/**
 * { "code": "0001", "gateway": { "gatewayId": "10000" }, "timestamp":
 * 1414565197 }/r/n
 *
 */
public class GatewayRegister0001UpRequest extends Request {

	private static final long serialVersionUID = 6551055735817404125L;

	private static final String CODE = "0001";

	@Override
	public String code() {
		return CODE;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("CODE", CODE).toString();
	}

}
