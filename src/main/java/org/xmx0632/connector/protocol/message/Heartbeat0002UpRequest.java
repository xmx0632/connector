package org.xmx0632.connector.protocol.message;

import com.google.common.base.MoreObjects;

/**
 * 网关与共性平台建正常注册后每分钟内发送一次“心跳”。共性平台通过“心跳”判断设备是否
 * 在线，如果一分钟内收不到设备的“心跳”，共性平台认为“设备不在线”，用户不能做进一步的设 备控制。网关在心跳包发出后 10
 * 秒内未收到共性平台的响应则认为网关与云平台失去连接，断开当 前 TCP 连接后重新发起连接。
 * 
 * { "version": "0.0.1","code": "0002", "gateway": { "gatewayId": "10000"},
 * "timestamp": 1414565197}/r/n
 * 
 */
// 网关→共性平台
public class Heartbeat0002UpRequest extends Request {

	private static final long serialVersionUID = -1960406391582094219L;
	private static final String CODE = "0002";

	@Override
	public String code() {
		return CODE;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("CODE", CODE).toString();
	}
	
	
}
