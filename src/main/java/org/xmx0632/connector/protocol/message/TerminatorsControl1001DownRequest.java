package org.xmx0632.connector.protocol.message;

import java.util.List;

import com.google.common.collect.Lists;
import com.google.common.base.MoreObjects;

/**
 * TODO 命令码： 1001
 * 
 * { "code": "1001", "sequence": "a89e91", "gateway": { "gatewayId": "10000" },
 * "deviceList": [ { "deviceId": "AABB", "control": [ { "atnumber": "0001",
 * "value": "20151225" //字符型 }, { "atnumber": "0002", "value": 75.3 //数值型 } ] },
 * { "deviceId": "CCDD", "control": [ { "atnumber": "0001", "attribute":
 * "switch", "value": "20151225"}, { "atnumber": "0002", "attribute":
 * "lightness", "value": 75.3 } ] } ], "timestamp": 1414565197 }/r/n
 *
 */
public class TerminatorsControl1001DownRequest extends Request {

	private static final long serialVersionUID = -3791290969524525816L;
	private String sequence = "";
	private Gateway gateway;
	private List<ControlDevice> deviceList = Lists.newArrayList();
	private long timestamp;

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	public Gateway getGateway() {
		return gateway;
	}

	public void setGateway(Gateway gateway) {
		this.gateway = gateway;
	}

	public List<ControlDevice> getDeviceList() {
		return deviceList;
	}

	public void setDeviceList(List<ControlDevice> deviceList) {
		this.deviceList = deviceList;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("code", code).add("sequence", sequence).add("gateway", gateway)
				.add("deviceList", deviceList).add("timestamp", timestamp).toString();
	}

	@Override
	public String code() {
		return "1001";
	}

}

class ControlDevice {
	private String deviceId;
	private List<AtNumber> control = Lists.newArrayList();

	public ControlDevice() {
	}

	public ControlDevice(String deviceId, List<AtNumber> control) {
		this.deviceId = deviceId;
		this.control = control;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public List<AtNumber> getControl() {
		return control;
	}

	public void setControl(List<AtNumber> control) {
		this.control = control;
	}

}