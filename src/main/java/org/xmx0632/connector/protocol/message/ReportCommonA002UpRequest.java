package org.xmx0632.connector.protocol.message;

import com.google.common.base.MoreObjects;

/**
 * 
 * TODO 终端设备一般消息上报
 * 
 * 命令码： 0xA002
 * 
 * 功能： 终端一般性状态信息上报
 * 
 * 方向： 网关→共性平台
 *
 */
public class ReportCommonA002UpRequest extends Request {

	private static final long serialVersionUID = -127936183528570892L;

	private static final String CODE = "A002";

	@Override
	public String code() {
		return CODE;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("CODE", CODE).toString();
	}

}
