package org.xmx0632.connector.protocol.message;

import com.google.common.base.MoreObjects;

public class DefaultResponse extends Response {

	private static final long serialVersionUID = 1L;
	
	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("code", code).toString();
	}

	@Override
	public String code() {
		return "-1";
	}


}
