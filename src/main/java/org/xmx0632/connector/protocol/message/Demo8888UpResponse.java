package org.xmx0632.connector.protocol.message;

public class Demo8888UpResponse extends Response {

	private static final long serialVersionUID = 6343360865680955582L;

	private static final String CODE = "-8888";

	@Override
	public String code() {
		return CODE;
	}
}
