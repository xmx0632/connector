package org.xmx0632.connector.protocol.wrapper;

import com.google.common.base.MoreObjects;

public class Gateway {
	private String gatewayId;

	public Gateway() {
	}

	public Gateway(String gatewayId) {
		this.gatewayId = gatewayId;
	}

	public String getGatewayId() {
		return gatewayId;
	}

	public void setGatewayId(String gatewayId) {
		this.gatewayId = gatewayId;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("gatewayId", gatewayId).toString();
	}

}