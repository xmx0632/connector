package org.xmx0632.connector.protocol.wrapper;

import com.google.common.base.MoreObjects;

public class AesWrapper {
	private Gateway gateway;
	private String data;

	public Gateway getGateway() {
		return gateway;
	}

	public void setGateway(Gateway gateway) {
		this.gateway = gateway;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("gateway", gateway).add("data", data).toString();
	}

}