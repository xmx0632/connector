package org.xmx0632.connector.server;

import java.net.InetSocketAddress;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;

@Component
public class AesServer {

	private static final Logger LOG = LoggerFactory.getLogger(AesServer.class);

	@Autowired
	@Qualifier("aesServerBootstrap")
	private ServerBootstrap aesServerBootstrap;

	@Autowired
	@Qualifier("aesSocketAddress")
	private InetSocketAddress aesPort;

	private ChannelFuture aesServerChannelFuture;

	@PostConstruct
	public void start() throws Exception {
		LOG.info("Starting aes server at " + aesPort);
		aesServerChannelFuture = aesServerBootstrap.bind(aesPort).sync();
	}

	@PreDestroy
	public void stop() throws Exception {
		aesServerChannelFuture.channel().closeFuture().sync();
	}

}
