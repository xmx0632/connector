package org.xmx0632.connector.server;

import java.net.InetSocketAddress;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;

@Component
public class TCPServer {

	private static final Logger LOG = LoggerFactory.getLogger(TCPServer.class);

	@Autowired
	@Qualifier("tcpServerBootstrap")
	private ServerBootstrap tcpServerBootstrap;

	@Autowired
	@Qualifier("tcpSocketAddress")
	private InetSocketAddress tcpPort;

	private ChannelFuture tcpServerChannelFuture;

	@PostConstruct
	public void start() throws Exception {
		LOG.info("Starting tcp server at " + tcpPort);
		tcpServerChannelFuture = tcpServerBootstrap.bind(tcpPort).sync();

	}

	@PreDestroy
	public void stop() throws Exception {
		tcpServerChannelFuture.channel().closeFuture().sync();
	}
}
