package org.xmx0632.connector.server;

import java.net.InetSocketAddress;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;

@Component
public class SslServer {

	private static final Logger LOG = LoggerFactory.getLogger(SslServer.class);

	@Autowired
	@Qualifier("sslServerBootstrap")
	private ServerBootstrap sslServerBootstrap;

	@Autowired
	@Qualifier("sslSocketAddress")
	private InetSocketAddress sslPort;

	private ChannelFuture sslServerChannelFuture;

	@PostConstruct
	public void start() throws Exception {

		LOG.info("Starting ssl server at " + sslPort);
		sslServerChannelFuture = sslServerBootstrap.bind(sslPort).sync();
	}

	@PreDestroy
	public void stop() throws Exception {
		sslServerChannelFuture.channel().closeFuture().sync();
	}

}
