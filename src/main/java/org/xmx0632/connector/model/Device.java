package org.xmx0632.connector.model;

import com.google.common.base.MoreObjects;

import io.netty.channel.Channel;

/**
 * 设备
 *
 */
public class Device {
	private String deviceId;

	private Channel channel;

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public Channel getChannel() {
		return channel;
	}

	public void setChannel(Channel channel) {
		this.channel = channel;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("deviceId", deviceId).toString();
	}

}
