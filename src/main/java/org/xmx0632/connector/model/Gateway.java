package org.xmx0632.connector.model;

import com.google.common.base.MoreObjects;

import io.netty.channel.Channel;
import com.google.common.base.Objects;

/**
 * 网关
 *
 */
public class Gateway {

	private String gatewayId;

	private Channel channel;

	// 超时时间,网关第一次连接成功后计算出来,用于下次心跳超时检测,收到心跳消息后重新计算
	private long expiredTimestamp;

	public Gateway(Channel incoming) {
		this.channel = incoming;
	}

	public String getGatewayId() {
		return gatewayId;
	}

	public void setGatewayId(String gatewayId) {
		this.gatewayId = gatewayId;
	}

	public long getExpiredTimestamp() {
		return expiredTimestamp;
	}

	public void setExpiredTimestamp(long expiredTimestamp) {
		this.expiredTimestamp = expiredTimestamp;
	}

	public Channel getChannel() {
		return channel;
	}

	public void setChannel(Channel channel) {
		this.channel = channel;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(gatewayId, channel, expiredTimestamp);
	}

	@Override
	public boolean equals(Object object) {
		if (object instanceof Gateway) {
			Gateway that = (Gateway) object;
			return Objects.equal(this.gatewayId, that.gatewayId) && Objects.equal(this.channel, that.channel)
					&& Objects.equal(this.expiredTimestamp, that.expiredTimestamp);
		}
		return false;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("gatewayId", gatewayId).add("channel", channel)
				.add("expiredTimestamp", expiredTimestamp).toString();
	}

}
