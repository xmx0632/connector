package org.xmx0632.connector.codec;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.xmx0632.connector.codec.JsonMessageEncoder;
import org.xmx0632.connector.protocol.JsonMessageParser;
import org.xmx0632.connector.protocol.message.Demo8888UpRequest;

import io.netty.channel.embedded.EmbeddedChannel;

public class JsonMessageEncoderTest {
	private String demoMessage = "{\"code\":\"8888\",\"timestamp\":0,\"version\":\"0.0.1\"}\r\n";
	private Demo8888UpRequest msg = new Demo8888UpRequest();

	@Test
	public void testEncodeChannelHandlerContextMessageListOfObject() {
		JsonMessageEncoder jsonMessageEncoder = new JsonMessageEncoder();
		jsonMessageEncoder.setJsonMessageParser(new JsonMessageParser());
		EmbeddedChannel channel = new EmbeddedChannel(jsonMessageEncoder);
		assertTrue(channel.writeOutbound(msg));
		assertTrue(channel.finish());
		String buf = (String) channel.readOutbound();
		assertEquals(demoMessage, buf);
		assertNull(channel.readOutbound());

	}

}
