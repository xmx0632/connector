package org.xmx0632.connector.codec;

import static org.junit.Assert.*;

import org.junit.Test;
import org.xmx0632.connector.codec.JsonMessageDecoder;
import org.xmx0632.connector.protocol.JsonMessageParser;
import org.xmx0632.connector.protocol.message.Demo8888UpRequest;

import io.netty.channel.embedded.EmbeddedChannel;

public class JsonMessageDecoderTest {

	@Test
	public void testClientWillBeDisconnectedWhenDecodeInvalidMessage() {
		String demoMessage = "{\"name\":\"demoMessage\"}\r\n";
		JsonMessageDecoder jsonMessageDecoder = new JsonMessageDecoder();
		jsonMessageDecoder.setJsonMessageParser(new JsonMessageParser());
		EmbeddedChannel channel = new EmbeddedChannel(jsonMessageDecoder);
		assertFalse(channel.writeInbound(demoMessage));
		assertFalse(channel.finish());
		Demo8888UpRequest message = (Demo8888UpRequest) channel.readInbound();
		assertNull(message);
	}

	@Test
	public void testDecodeDemoMessage() {
		String demoMessage = "{\"name\":\"8888demoMessage\"}\r\n";
		JsonMessageDecoder jsonMessageDecoder = new JsonMessageDecoder();
		jsonMessageDecoder.setJsonMessageParser(new JsonMessageParser());
		EmbeddedChannel channel = new EmbeddedChannel(jsonMessageDecoder);
		assertTrue(channel.writeInbound(demoMessage));
		assertTrue(channel.finish());
		Demo8888UpRequest message = (Demo8888UpRequest) channel.readInbound();
		assertEquals("8888", message.getCode());
		assertNull(channel.readInbound());
	}

	@Test
	public void testDecodeDemoMessage1() {
		String demoMessage = "{\"name\":\"8888\"}\r\n";
		JsonMessageDecoder jsonMessageDecoder = new JsonMessageDecoder();
		jsonMessageDecoder.setJsonMessageParser(new JsonMessageParser());
		EmbeddedChannel channel = new EmbeddedChannel(jsonMessageDecoder);
		assertTrue(channel.writeInbound(demoMessage));
		assertTrue(channel.finish());
		Demo8888UpRequest message = (Demo8888UpRequest) channel.readInbound();
		assertEquals("8888", message.getCode());
		assertNull(channel.readInbound());
	}
}
