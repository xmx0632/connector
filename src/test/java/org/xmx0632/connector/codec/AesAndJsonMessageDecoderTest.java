package org.xmx0632.connector.codec;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.xmx0632.connector.codec.AesMessageDecoder;
import org.xmx0632.connector.codec.JsonMessageDecoder;
import org.xmx0632.connector.protocol.JsonMessageParser;
import org.xmx0632.connector.protocol.message.Demo8888UpRequest;

import io.netty.channel.embedded.EmbeddedChannel;

public class AesAndJsonMessageDecoderTest {

	@Test
	public void testDecodeAesMessage() {
		String content = "8888";
		String wrapData = "{\"data\":\"" + content + "\",\"gateway\":{\"gatewayId\":\"10000\"}}\r\n";
		JsonMessageParser jsonMessageParser = new JsonMessageParser();
		AesMessageDecoder aesMessageDecoder = new AesMessageDecoder();
		aesMessageDecoder.setJsonMessageParser(jsonMessageParser);

		JsonMessageDecoder jsonMessageDecoder = new JsonMessageDecoder();
		jsonMessageDecoder.setJsonMessageParser(jsonMessageParser);
		EmbeddedChannel channel = new EmbeddedChannel(aesMessageDecoder, jsonMessageDecoder);
		assertTrue(channel.writeInbound(wrapData));
		assertTrue(channel.finish());
		Demo8888UpRequest message = (Demo8888UpRequest) channel.readInbound();
		assertEquals("8888", message.getCode());
		assertEquals("0.0.1", message.getVersion());
		assertNull(channel.readInbound());
	}

	
	@Test
	public void testDecodeAesMessage1() {
		String content = "0001";
		String wrapData = "{\"data\":\"" + content + "\",\"gateway\":{\"gatewayId\":\"10000\"}}\r\n";
		JsonMessageParser jsonMessageParser = new JsonMessageParser();
		AesMessageDecoder aesMessageDecoder = new AesMessageDecoder();
		aesMessageDecoder.setJsonMessageParser(jsonMessageParser);

		JsonMessageDecoder jsonMessageDecoder = new JsonMessageDecoder();
		jsonMessageDecoder.setJsonMessageParser(jsonMessageParser);
		EmbeddedChannel channel = new EmbeddedChannel(aesMessageDecoder, jsonMessageDecoder);
		assertTrue(channel.writeInbound(wrapData));
		assertTrue(channel.finish());
		Demo8888UpRequest message = (Demo8888UpRequest) channel.readInbound();
		assertEquals("8888", message.getCode());
		assertEquals("0.0.1", message.getVersion());
		assertNull(channel.readInbound());
	}

}
