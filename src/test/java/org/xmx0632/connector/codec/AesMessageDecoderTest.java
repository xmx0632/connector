package org.xmx0632.connector.codec;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.xmx0632.connector.codec.AesMessageDecoder;
import org.xmx0632.connector.protocol.JsonMessageParser;

import io.netty.channel.embedded.EmbeddedChannel;

public class AesMessageDecoderTest {

	@Test
	public void testDecodeAesMessage() {
		String wrapData = "{\"data\":\"1234567890\",\"gateway\":{\"gatewayId\":\"10000\"}}";
		AesMessageDecoder aesMessageDecoder = new AesMessageDecoder();
		aesMessageDecoder.setJsonMessageParser(new JsonMessageParser());
		EmbeddedChannel channel = new EmbeddedChannel(aesMessageDecoder);
		assertTrue(channel.writeInbound(wrapData));
		assertTrue(channel.finish());
		String message = (String) channel.readInbound();
		assertEquals("1234567890", message.toString());
		assertNull(channel.readInbound());
	}

}
