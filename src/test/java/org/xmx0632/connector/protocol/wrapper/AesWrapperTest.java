package org.xmx0632.connector.protocol.wrapper;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.xmx0632.connector.protocol.wrapper.AesWrapper;
import org.xmx0632.connector.protocol.wrapper.Gateway;

import com.alibaba.fastjson.JSON;

public class AesWrapperTest {

	private static final String DATA = "xxxx";
	private static final String GATEWAY_ID = "100000";

	@Test
	public void testToJson() {
		AesWrapper wrapper = new AesWrapper();
		wrapper.setData(DATA);
		Gateway gateway = new Gateway("11111");
		gateway.setGatewayId(GATEWAY_ID);
		wrapper.setGateway(gateway);

		String string = JSON.toJSONString(wrapper);
		System.out.println(string);
		String expected = wrapData();
		assertEquals(expected, string);
	}

	@Test
	public void testFromJson() {
		String string = wrapData();
		AesWrapper msg = JSON.parseObject(string, AesWrapper.class);
		System.out.println(msg);
		assertEquals(DATA, msg.getData());
		assertEquals(GATEWAY_ID, msg.getGateway().getGatewayId());
	}

	private String wrapData() {
		return "{\"data\":\"" + DATA + "\",\"gateway\":{\"gatewayId\":\"" + GATEWAY_ID + "\"}}";
	}
}
