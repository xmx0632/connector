package org.xmx0632.connector.protocol.message;

import static org.junit.Assert.*;

import org.junit.Test;
import org.xmx0632.connector.protocol.message.Demo8888UpRequest;

public class DemoRequestTest extends BaseRequestTest {

	@Test
	public void testToJson() {
		Demo8888UpRequest msg = new Demo8888UpRequest();

		String string = toJsonString(msg);
		System.out.println(string);
		String expected = "{\"code\":\"8888\",\"timestamp\":0,\"version\":\"0.0.1\"}";
		assertEquals(expected, string);
	}

	@Test
	public void testFromJson() {
		String string = "{\"code\":\"8888\"}";
		Demo8888UpRequest message = toObject(string, Demo8888UpRequest.class);
		System.out.println(message);
		assertEquals("8888", message.getCode());
		assertEquals("0.0.1", message.getVersion());
	}

	
	@Test
	public void testFromJson1() {
		String string = "{\"code\":\"8888\",\"timestamp\":123213213210,\"version\":\"0.0.1\"}";
		Demo8888UpRequest message = toObject(string, Demo8888UpRequest.class);
		System.out.println(message);
		assertEquals("8888", message.getCode());
		assertEquals("0.0.1", message.getVersion());
		assertEquals(123213213210l, message.getTimestamp());
	}
}
