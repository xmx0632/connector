package org.xmx0632.connector.protocol.message;

import org.xmx0632.connector.protocol.message.Message;

import com.alibaba.fastjson.JSON;

public class BaseRequestTest {

	public BaseRequestTest() {
		super();
	}

	protected String toJsonString(Message msg) {
		return JSON.toJSONString(msg);
	}

	protected <T> T toObject(String string, Class<T> class1) {
		T message = JSON.parseObject(string, class1);
		return message;
	}

}