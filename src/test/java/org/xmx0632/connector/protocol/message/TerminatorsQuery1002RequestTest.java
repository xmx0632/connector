package org.xmx0632.connector.protocol.message;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.xmx0632.connector.protocol.message.AtNumber;
import org.xmx0632.connector.protocol.message.Gateway;
import org.xmx0632.connector.protocol.message.QueryDevice;
import org.xmx0632.connector.protocol.message.TerminatorsQuery1002DownRequest;

import com.google.common.collect.Lists;

public class TerminatorsQuery1002RequestTest extends BaseRequestTest {

	@Test
	public void testToJson() {
		TerminatorsQuery1002DownRequest req = new TerminatorsQuery1002DownRequest();
		String string = toJsonString(req);
		System.out.println(string);
		String expected = "{\"code\":\"1002\",\"deviceList\":[],\"sequence\":\"\",\"timestamp\":0,\"version\":\"0.0.1\"}";
		assertEquals(expected, string);
	}

	@Test
	public void testToJson1() {
		TerminatorsQuery1002DownRequest req = new TerminatorsQuery1002DownRequest();
		req.setGateway(new Gateway("10000"));
		req.setSequence("a89e91");
		req.setTimestamp(1451378699250l);

		List<QueryDevice> deviceList = Lists.newArrayList();
		QueryDevice queryDevice = new QueryDevice();
		queryDevice.setDeviceId("AABB");
		List<AtNumber> query = Lists.newArrayList();
		query.add(new AtNumber("0001"));
		query.add(new AtNumber("0002"));
		queryDevice.setQuery(query);
		deviceList.add(queryDevice);

		QueryDevice queryDevice2 = new QueryDevice();
		queryDevice2.setDeviceId("CCDD");
		List<AtNumber> query2 = Lists.newArrayList();
		query2.add(new AtNumber("0001"));
		query2.add(new AtNumber("0002"));
		queryDevice2.setQuery(query2);
		deviceList.add(queryDevice2);

		req.setDeviceList(deviceList);

		String string = toJsonString(req);
		System.out.println(string);
		String expected = "{\"code\":\"1002\",\"deviceList\":[{\"deviceId\":\"AABB\",\"query\":[{\"atnumber\":\"0001\"},{\"atnumber\":\"0002\"}]},{\"deviceId\":\"CCDD\",\"query\":[{\"atnumber\":\"0001\"},{\"atnumber\":\"0002\"}]}],\"gateway\":{\"gatewayId\":\"10000\"},\"sequence\":\"a89e91\",\"timestamp\":1451378699250,\"version\":\"0.0.1\"}";
		assertEquals(expected, string);
	}

	@Test
	public void testToJson2() {
		TerminatorsQuery1002DownRequest req = new TerminatorsQuery1002DownRequest();
		req.setGateway(new Gateway("10000"));
		req.setSequence("a89e91");
		req.setTimestamp(1451378699250l);

		List<QueryDevice> deviceList = Lists.newArrayList();
		QueryDevice queryDevice = new QueryDevice();
		queryDevice.setDeviceId("AABB");
		List<AtNumber> query = Lists.newArrayList();
		query.add(new AtNumber("0001"));
		query.add(new AtNumber("0002"));
		queryDevice.setQuery(query);
		deviceList.add(queryDevice);

		req.setDeviceList(deviceList);

		String string = toJsonString(req);
		System.out.println(string);
		String expected = "{\"code\":\"1002\",\"deviceList\":[{\"deviceId\":\"AABB\",\"query\":[{\"atnumber\":\"0001\"},{\"atnumber\":\"0002\"}]}],\"gateway\":{\"gatewayId\":\"10000\"},\"sequence\":\"a89e91\",\"timestamp\":1451378699250,\"version\":\"0.0.1\"}";
		assertEquals(expected, string);
	}

	@Test
	public void testFromJson() {
		String string = "{\"code\":\"1002\",\"deviceList\":[{\"deviceId\":\"AABB\",\"query\":[{\"atnumber\":\"0001\"},{\"atnumber\":\"0002\"}]},{\"deviceId\":\"CCDD\",\"query\":[{\"atnumber\":\"0001\"},{\"atnumber\":\"0002\"}]}],\"gateway\":{\"gatewayId\":\"10000\"},\"sequence\":\"a89e91\",\"timestamp\":1451378699250}";

		TerminatorsQuery1002DownRequest msg = toObject(string, TerminatorsQuery1002DownRequest.class);

		assertEquals("1002", msg.getCode());
		assertEquals("10000", msg.getGateway().getGatewayId());
		assertEquals("a89e91", msg.getSequence());
		assertEquals(1451378699250l, msg.getTimestamp());
		assertEquals(2, msg.getDeviceList().size());
	}

	@Test
	public void testFromJson1() {
		String string = "{\"code\":\"1002\",\"deviceList\":[{\"deviceId\":\"AABB\",\"query\":[{\"atnumber\":\"0001\"},{\"atnumber\":\"0002\"}]}],\"gateway\":{\"gatewayId\":\"10000\"},\"sequence\":\"a89e91\",\"timestamp\":1451378699250}";

		TerminatorsQuery1002DownRequest msg = toObject(string, TerminatorsQuery1002DownRequest.class);

		assertEquals("1002", msg.getCode());
		assertEquals("10000", msg.getGateway().getGatewayId());
		assertEquals("a89e91", msg.getSequence());
		assertEquals(1451378699250l, msg.getTimestamp());
		assertEquals(1, msg.getDeviceList().size());
		QueryDevice queryDevice = msg.getDeviceList().get(0);
		assertEquals("AABB", queryDevice.getDeviceId());
		assertEquals(2, queryDevice.getQuery().size());
	}
}
