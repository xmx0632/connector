package org.xmx0632.connector.protocol.message;

import static org.junit.Assert.*;

import org.junit.Test;
import org.xmx0632.connector.protocol.message.Heartbeat0002UpResponse;

public class Heartbeat0002UpResponseTest extends BaseRequestTest {

	@Test
	public void testToJson() {
		Heartbeat0002UpResponse msg = new Heartbeat0002UpResponse();

		String string = toJsonString(msg);
		System.out.println(string);
		String expected = "{\"code\":\"8002\",\"result\":0,\"timestamp\":0,\"version\":\"0.0.1\"}";
		assertEquals(expected, string);
	}

	@Test
	public void testFromJson() {
		String string = "{\"code\":\"8002\"}";
		Heartbeat0002UpResponse message = toObject(string, Heartbeat0002UpResponse.class);
		System.out.println(message);
		assertEquals("8002", message.getCode());
		assertEquals("0.0.1", message.getVersion());
	}

	@Test
	public void testFromJson1() {
		String string = "{\"code\":\"8002\",\"timestamp\":123213213210,\"version\":\"0.0.1\"}";
		Heartbeat0002UpResponse message = toObject(string, Heartbeat0002UpResponse.class);
		System.out.println(message);
		assertEquals("8002", message.getCode());
		assertEquals("0.0.1", message.getVersion());
		assertEquals(123213213210l, message.getTimestamp());
	}

}
