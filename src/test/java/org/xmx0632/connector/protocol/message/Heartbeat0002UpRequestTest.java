package org.xmx0632.connector.protocol.message;

import static org.junit.Assert.*;

import org.junit.Test;
import org.xmx0632.connector.protocol.message.Gateway;
import org.xmx0632.connector.protocol.message.Heartbeat0002UpRequest;

public class Heartbeat0002UpRequestTest extends BaseRequestTest {

	@Test
	public void testToJsonVersion1() {
		Heartbeat0002UpRequest msg = new Heartbeat0002UpRequest();
		msg.setVersion("0.0.1");
		String string = toJsonString(msg);
		System.out.println(string);
		String expected = "{\"code\":\"0002\",\"timestamp\":0,\"version\":\"0.0.1\"}";
		assertEquals(expected, string);
	}

	@Test
	public void testToJsonVersion2() {
		Heartbeat0002UpRequest msg = new Heartbeat0002UpRequest();
		msg.setVersion("0.0.2");
		msg.setGateway(new Gateway("100000"));
		String string = toJsonString(msg);
		System.out.println(string);
		String expected = "{\"code\":\"0002\",\"gateway\":{\"gatewayId\":\"100000\"},\"timestamp\":0,\"version\":\"0.0.2\"}";
		assertEquals(expected, string);
	}

	@Test
	public void testFromJson() {
		String string = "{\"code\":\"0002\"}";
		Heartbeat0002UpRequest message = toObject(string, Heartbeat0002UpRequest.class);
		System.out.println(message);
		assertEquals("0002", message.getCode());
		assertEquals("0.0.1", message.getVersion());
	}

	@Test
	public void testFromJson1() {
		String string = "{\"code\":\"0002\",\"gateway\":{\"gatewayId\":\"100000\"},\"version\":\"0.0.2\"}";
		Heartbeat0002UpRequest message = toObject(string, Heartbeat0002UpRequest.class);
		System.out.println(message);
		assertEquals("0002", message.getCode());
		assertEquals("0.0.2", message.getVersion());
		assertEquals("100000", message.getGateway().getGatewayId());
	}
	
	@Test
	public void testFromJson2() {
		String string = "{\"code\":\"0002\",\"gateway\":{\"gatewayId\":\"100000\"},\"timestamp\":233333232332,\"version\":\"0.0.2\"}";
		Heartbeat0002UpRequest message = toObject(string, Heartbeat0002UpRequest.class);
		System.out.println(message);
		assertEquals("0002", message.getCode());
		assertEquals("0.0.2", message.getVersion());
		assertEquals(233333232332l, message.getTimestamp());
		assertEquals("100000", message.getGateway().getGatewayId());
	}
}
