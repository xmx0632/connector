启动命令:
java -Xms1024m -Xmx1024m -XX:PermSize=256M -XX:MaxPermSize=256M -jar connector-0.0.1-SNAPSHOT.jar


开发说明:

消息解析:

JsonMessageParser:
把String解析为Json对象(位于org.xmx0632.connector.protocol.message),
参考org.xmx0632.connector.protocol.message.DemoMessage

解析出DemoMessage后自动查找DemoMessageHandler处理DemoMessage消息.
参考org.xmx0632.connector.protocol.handler.DemoMessageHandler

需要解析新的json消息时需要在org.xmx0632.connector.protocol.message下新增一个与json对应的java对象.

