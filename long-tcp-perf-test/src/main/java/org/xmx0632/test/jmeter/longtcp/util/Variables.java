package org.xmx0632.test.jmeter.longtcp.util;

public class Variables {
	private SocketClient client;
	private long timestamp;

	public SocketClient getClient() {
		return this.client;
	}

	public void setClient(SocketClient client) {
		this.client = client;
	}

	public long getTimestamp() {
		return this.timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
}
